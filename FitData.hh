#ifndef FITDATA_HH
#define FITDATA_HH
#define WEIGHT_LUT_LENGTH 1001
#define LUT_WIDTH 3500
#define LUT_LENGTH (2*LUT_WIDTH+1)
#define FAST_LUT_WIDTH 4095
#define FAST_LUT_LENGTH (2*FAST_LUT_WIDTH+1)
#define inverse_lut_interval 1000.f
#define inverse_weight_lut_interval  1000.f
class FitDataFloat
{
public:
  static const float binomial_weight[];
  static const float data_logx[];
  static const float data_errf[];
};
#endif

//#endif

