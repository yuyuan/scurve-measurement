#ifndef RCEFITTER_HH
#define RCEFITTER_HH

#include <vector>

typedef struct
{
  int converge;
  double chi2;
} FitResult;
 

class  FitScurveLikelihoodFloat {
public:

  typedef struct
  {
    float mu, sigma, a0;
  } GaussianCurve;

  
  typedef struct {
    float deltaMu, deltaSigma; /* user provides initial deltaMu and deltaSigma */
    float muEpsilon, sigmaEpsilon; /* user provides convergence criterion */
    unsigned nIters;
    unsigned ndf; /* number of degrees of freedom */
    float chi2; /* final result for chi2 */
    bool converge;
    float muConverge,sigmaConverge;
    unsigned maxIters;
    void *curve;
  } Fit;
  
  typedef struct
  {
    unsigned int n;
    const float *x;
    const float *y;
  } FitData;
  
  
public:
  FitScurveLikelihoodFloat(unsigned nTrigger);
  //bool doFit(const std::vector<float> &x,const std::vector<float> &y,float &mu, float &sigma  );
  FitResult doFit(const std::vector<float> &x,const std::vector<float> &y,float &mu, float &sigma  );
  ~FitScurveLikelihoodFloat();
  void initOCM();
//private:
  inline float *log_ext(float x, GaussianCurve *psp);
  float errf_ext(float x, GaussianCurve *psp);
  float logLikelihood(FitData *pfd, GaussianCurve *s);
  void initFit(Fit *pFit);
  unsigned extractGoodData(FitData *pfdi,FitData *pfdo, float scanA0);
  void initialGuess(FitData *pfd, GaussianCurve *pSParams);
  bool fitPixel(FitData *pfd,void *vpfit);
  float chiSquared(FitData *pfd,GaussianCurve *s);
  Fit m_fit;
  unsigned m_a0;
  bool useOCM;
  void *ocm_addr;
  const float *data_logx;
  int fd;
public:
  float *x_buf;
  float *y_buf;
};

#endif
