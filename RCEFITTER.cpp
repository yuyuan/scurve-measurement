#include "RCEFITTER.hh"
#include "FitData.hh"
#include <stdio.h>
#include <iostream> 
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/mman.h>

#if ARMV8SIMD==1
#include <arm_neon.h>
#endif

#define MAP_SIZE (4*64*1024)
#define MAP_MASK (MAP_SIZE - 1)
#define DEBUG false

FitScurveLikelihoodFloat::FitScurveLikelihoodFloat(unsigned a0) : m_a0(a0),useOCM(false),ocm_addr(0),data_logx(FitDataFloat::data_logx)     {
  initFit(&m_fit);
}

void FitScurveLikelihoodFloat::initOCM() {
#if ARMV8SIMD==1
  void *map_base=0; 
  unsigned long read_result, writeval;
  if((fd = open("/dev/mem", O_RDWR | O_SYNC)) != -1) {
    off_t  ocm_base=0xfffd000;

    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, ocm_base & ~MAP_MASK);
    if(map_base != (void *) -1) {;
      ocm_addr =map_base + (ocm_base & MAP_MASK);
      useOCM=true;
    }    
  }
  if(useOCM) {
    float *p=(float *) ocm_addr;
    for(unsigned i=0;i<2*LUT_LENGTH;i++) {
      p[i]=FitDataFloat::data_logx[i];
    }
    data_logx=p;
    x_buf=&p[2*LUT_LENGTH];
    y_buf=&p[2*LUT_LENGTH+8192];
    
  }
#else
	std::cout<<"FitScurveLikelihoodFloat::initOCM is disabled"<<std::endl;
	return;  
#endif

}


FitScurveLikelihoodFloat::~FitScurveLikelihoodFloat()
{
  if(useOCM) close(fd);
}

float* FitScurveLikelihoodFloat::log_ext(float x, GaussianCurve *psp) {
  float u, t;
  int n;
  u = (x - psp->mu) / psp->sigma;
  t = u * inverse_lut_interval;
  n = LUT_WIDTH + (int)t;
  if(n < 0) n = 0;
  if(n >= LUT_LENGTH) n = LUT_LENGTH-1; // truncate at last value
  n = 2 * n;
  return (float*)&data_logx[n];
}

float FitScurveLikelihoodFloat::errf_ext(float x, GaussianCurve *psp) {
  float u, t;
  int n;
  
  u = (x - psp->mu) / psp->sigma;
  t = u * inverse_lut_interval;
  n = LUT_WIDTH + (int)t;
  if(n < 0) n = 0;
  if(n >= LUT_LENGTH) n = LUT_LENGTH-1; // truncate at last value 
  return FitDataFloat::data_errf[n];
}

float FitScurveLikelihoodFloat::logLikelihood(FitData *pfd, GaussianCurve *s) 
{
  #if ARMV8SIMD==1
  float acc, r, y2, y3, N;
  const float *x, *y;
  float p,q;
  int k, n;
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0.0f;
  N = s->a0;

  float u, t;
  int nl;
  float invSigma= 1./s->sigma;
  float mu_sigma= s->mu / s->sigma;
  unsigned n4=n/4;
  unsigned n4_r=n%4;
  float32x4_t invSigma4=vmovq_n_f32(invSigma);
  float32x4_t mu_sigma4=vmovq_n_f32(mu_sigma);
  int32x4_t zero=vmovq_n_s32(0);
  uint32x4_t two=vmovq_n_u32(2);
  float32x4_t inverse_lut_interval4=vmovq_n_f32(inverse_lut_interval);
  int32x4_t LUT_WIDTH4=vmovq_n_s32( LUT_WIDTH );
  int32x4_t LUT_LENGTH4=vmovq_n_s32( LUT_LENGTH );
  uint32x4_t LUT_LENGTH_1_4=vmovq_n_u32( LUT_LENGTH-1 );
  float32x4_t N4=vmovq_n_f32(N);
  for(k=0;k<n4;++k) {
    float32x4_t x4=vld1q_f32(x);x+=4;
    float32x4_t y4=vld1q_f32(y);y+=4;
    float32x4_t u4=vmulq_f32(x4,invSigma4);
    u4=vsubq_f32(u4,mu_sigma4);
    float32x4_t t4=vmulq_f32(u4,inverse_lut_interval4);
    int32x4_t nl4=vcvtq_s32_f32(t4);
    nl4=vaddq_s32(nl4,LUT_WIDTH4);
    uint32x4_t cond0=vcgeq_s32(nl4,zero);
    uint32x4_t cond1=vcleq_s32(nl4,LUT_LENGTH4);
    uint32x4_t cond2=vmvnq_u32(cond1);
    cond0=vandq_u32(cond0,cond1);
    cond0=vandq_u32(cond0,two);
    cond2=vandq_u32(cond2,two);
    uint32x4_t nu4= vreinterpretq_u32_s32(nl4); 
    nu4=vmulq_u32(cond0,nu4);
    uint32x4_t temp=vmulq_u32( LUT_LENGTH_1_4,cond2);
    nu4=vaddq_u32(nu4,temp);
    float32x4_t p,q;
    for(unsigned i=0;i<4;i++) {
      p[i]=data_logx[nu4[i]];
      q[i]=data_logx[nu4[i]+1];
     }
    float32x4_t y2,y3;
    float32x4_t acc4;
    y3=vsubq_f32(N4,y4);
    y2=vmulq_f32(y4,p);
    y3=vmulq_f32(y3,q);
    acc4=vaddq_f32(y2,y3);
    float32x2_t temp1 = vadd_f32(vget_high_f32(acc4), vget_low_f32(acc4));
    float32x2_t temp2 = vpadd_f32(temp1, temp1);
    float sum = vget_lane_f32(temp2, 0);

    acc-=sum;
  } 

  for(k=0;k<n4_r;++k) {
    u = (*x)*invSigma-mu_sigma;
    x++;
    t = u * inverse_lut_interval;
    nl = LUT_WIDTH + (int)t;
    //    if(nl < 0) nl = 0;
    //  if(nl >= LUT_LENGTH) nl = LUT_LENGTH-1; // truncate at last value   
    unsigned cond0=(nl>=0 and nl<LUT_LENGTH)*2;
    unsigned cond1=(nl>=LUT_LENGTH)*2;
    //    std::cout << nl << " " <<cond0 << " " << cond1 << " " << cond2 << std::endl;
    nl=cond1*(LUT_LENGTH-1)+(nl*cond0);

    //    nl = nl<<1;
    p=data_logx[nl];
    q=data_logx[nl+1];
    r = *y; /* data point */
    y++;
    y2 = r * p; /* log(prob) */
    y3 = (N - r) * q; /* log(1-prob) */
    acc -= (y2 + y3);
  }

  return acc;
#else

  float acc, r, y2, y3, N;
  const float *x, *y;
  float *p;
  int k, n;
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0.0f;
  N = s->a0;
  for(k=0;k<n;++k) {
    r = *y++; /* data point */
    p = log_ext(*x++,s); /* pointer to log(probability) */
    y2 = r * (*p++); /* log(prob) */
    y3 = (N - r) * (*p); /* log(1-prob) */
    acc -= (y2 + y3);
  }
  return acc;
#endif

}


//bool FitScurveLikelihoodFloat::doFit(const std::vector<float> &x ,const std::vector<float> &y,float &mu,float &sigma)
FitResult FitScurveLikelihoodFloat::doFit(const std::vector<float> &x ,const std::vector<float> &y,float &mu,float &sigma)
{

  FitData pfdi,pfdo;
  FitResult out;
  out.converge=0;
  out.chi2=0;
  GaussianCurve gc;
  pfdi.n=x.size();
  pfdi.x=x.data(); 
  pfdi.y=y.data();
  unsigned goodBins=extractGoodData(&pfdi,&pfdo,m_a0);
  #if DEBUG
  std::cout << "after extragooddata" << std::endl;
  std::cout << "goodBins:" << goodBins <<std::endl;
  #endif
  //if(goodBins<5) return false;
  if(goodBins<5) return out;
  m_fit.curve=&gc;
  m_fit.maxIters=10000;
  gc.a0=m_a0;

  bool result=fitPixel(&pfdo,&m_fit);  
  mu=gc.mu;
  sigma=gc.sigma;
  //std::cout<<"chi2="<<m_fit.chi2<<std::endl;
  if(result){ out.converge=1; }else{ out.converge=0; }
  out.chi2=m_fit.chi2;
  return out;
}


void FitScurveLikelihoodFloat::initFit(Fit *pFit) {
  pFit->deltaSigma = 0.1f;
  pFit->deltaMu = 0.1f;
  pFit->maxIters = 10000;
  pFit->muEpsilon = 0.0001f; /* 0.01% */
  pFit->sigmaEpsilon = 0.0001f; /* 0.01% */
}
unsigned FitScurveLikelihoodFloat::extractGoodData(FitData *pfdi,FitData *pfdo, float scanA0) {
  int hitsStart, hitsEnd, nBins;
  float a0;
  const float *y;
  
  nBins = pfdi->n;
  y = pfdi->y;
  
  //new method to find hitsStart  
  for(hitsStart = nBins-1;hitsStart >= 0; --hitsStart)
    if(y[hitsStart] == 0) break;  
  #if DEBUG
  std::cout<<"FitScurveLikelihoodFloat::extractGoodData hitsStart="<<hitsStart<<std::endl;
  std::cout<<"y="<<y[hitsStart]<<std::endl;
  #endif 
  if(hitsStart ==0)
    return 0;
  if(hitsStart == nBins)
    return 0;  
  a0 = 0.999f * y[nBins - 1]; // just under last bin 
  #if DEBUG
  std::cout<<"a0="<<a0<<std::endl;
  std::cout<<"scanA0="<<scanA0<<std::endl;
  #endif
  if(a0 < 0.9 * scanA0)
    return 0;
  //failure mode, never reaches A0

  for(hitsEnd = hitsStart; hitsEnd < nBins; ++hitsEnd)
    if(y[hitsEnd] > a0) break;
  nBins = 1 + hitsEnd - hitsStart;
  #if DEBUG
  std::cout<<"FitScurveLikelihoodFloat::extractGoodData   nBins="<<nBins<<"; hitsStart="<<hitsStart<<"; hitsEnd="<<hitsEnd<<std::endl;
  #endif
  if(hitsStart < 0)
    hitsStart =0;
  
  if(hitsStart == hitsEnd)
    return 0;
  
  if(nBins < 2) {
    return 0;
    //failure mode :: not enough data points
  }
  if(nBins < 0)  nBins = 0;

  pfdo->n = nBins;
  
  pfdo->y = &y[hitsStart];
  pfdo->x = &pfdi->x[hitsStart];
  return nBins;
}

void FitScurveLikelihoodFloat::initialGuess(FitData *pfd, GaussianCurve *pSParams){
#define invRoot6f  0.40824829046f
#define invRoot2f  0.7071067811f
  float pt1, pt2, pt3;
  const float *pdlo, *pdhi;
  float y_lo, y_hi, x1, x2, x3, sigma;
  const float *x, *y;
  float a0, minSigma;
  int j, k, n, lo1, lo2, lo3, hi1, hi2, hi3;

  //pSParams->a0 = pfd->y[pfd->n-1]; // assumes last data point sets plateau 
  pSParams->a0 = m_a0;
  if(DEBUG)std::cout<<"pfd->n="<<pfd->n<<std::endl;
  if(DEBUG)std::cout<<"a0="<<a0<<" pfd->y:"<<pfd->y[pfd->n-2]<<", "<<pfd->y[pfd->n-1]<<std::endl;  
  a0 = pSParams->a0;
  a0 -= 0.00001f; // skim a token amount to ensure comparisons succeed 

  n = pfd->n; // number of samples 

  minSigma = (pfd->x[1] - pfd->x[0]) * invRoot6f;
 
    x = pfd->x; y = pfd->y;
    pt1 = 0.16f * a0;
    pt2 = 0.50f * a0;
    pt3 = 0.84f * a0;
        // find the range over which the data crosses the 16%, 50% and 84% points 
    hi1 = hi2 = hi3 = 0; // invalid values 
    lo1 = lo2 = lo3 = 0; // invalid values 
    pdlo = &y[0]; // y 
    pdhi = &y[n-1]; // y + n - 1 
    // important note: for the sake of speed we want to perform as few comparisons as
    //possible. Therefore, the integer comparison occurs first in each of the 
    //following operations. Depending on the logical value thereof, the next
    //comparison will be made only if necessary. To further expedite the code,
                //arrays are not used because there are only three members 
    j = n - 1;
    for(k=0;k<n;++pdlo, --pdhi) {
      y_lo = *pdlo;
      y_hi = *pdhi;
      if(!lo1 && (y_lo >= pt1)) lo1 = k;
      if(!lo2 && (y_lo >= pt2)) lo2 = k;
      if(!lo3 && (y_lo >= pt3)) lo3 = k;
                        if(!hi1 && (y_hi <= pt1)) hi1 = j;
                        if(!hi2 && (y_hi <= pt2)) hi2 = j;
                        if(!hi3 && (y_hi <= pt3)) hi3 = j;
                        --j;
                        ++k;
    }
    x1 = (x[lo1] + x[hi1]) * 0.5f;
    x2 = (x[lo2] + x[hi2]) * 0.5f;
    x3 = (x[lo3] + x[hi3]) * 0.5f;
    
    pSParams->mu = x2;
 
    sigma = (x3 - x1) * invRoot2f;

    if(sigma < minSigma) {
                        sigma = minSigma; 
    }
    pSParams->sigma=sigma;

	if(DEBUG)std::cout<<"FitScurveLikelihoodFloat::initialGuess: init mu="<<pSParams->mu<<", sigma="<<pSParams->sigma<<", a0="<<pSParams->a0<<std::endl;

}


bool FitScurveLikelihoodFloat::fitPixel(FitData *pfd,void *vpfit)
{
  #if DEBUG
  std::cout << "Entering FitScurveLikelihoodFloat::fitPixel" << std::endl;
  #endif
  float deltaSigma, deltaMu, sigma, chi2Min, *fptr;
  float chi2[9];
  GaussianCurve sParams, sParamsWork, *psp;
  int k, dirMin;
  Fit *pFit;
  pFit = (Fit *)vpfit;
  
  pFit->nIters = 0;
  pFit->converge = false; // assume no convergence 
  
  pFit->ndf = pfd->n - 2; // degrees of freedom 
  
  // take a guess at the S-curve parameters 
  initialGuess(pfd,&sParams);
  
  // initialize loop parameters 
  psp = &sParamsWork;
  psp->a0 = sParams.a0;
  deltaSigma = pFit->deltaSigma * sParams.sigma; // scaled 
  deltaMu = pFit->deltaMu * sParams.mu; // scaled 
  while(!pFit->converge && (pFit->nIters++ < pFit->maxIters)) {
    
    dirMin = 0;
    fptr = chi2;
    for(k=0;k<9;++k) *fptr++ = 0.0f;
    
    while((dirMin >= 0) && (pFit->nIters++ < pFit->maxIters)) {
      //* calculate neighboring points *
      //* calculate neighboring points *
      psp->sigma=sParams.sigma - deltaSigma;
	  if(DEBUG)std::cout<<"start: sParams=("<<sParams.mu<<", "<<sParams.sigma<<") deltaMu="<<deltaMu<<", deltaSigma="<<deltaSigma<<std::endl;
      psp->mu = sParams.mu - deltaMu;
	if(DEBUG)std::cout<<"FitScurveLikelihoodFloat::fitPixel:  mu="<<psp->mu<<", sigma="<<psp->sigma<<", a0="<<psp->a0<<std::endl;
      if(chi2[0] == 0.0f)
        chi2[0] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[7] == 0.0f)
        chi2[7] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[6] == 0.0f)
        chi2[6] = logLikelihood(pfd,psp);
      
      psp->sigma=sParams.sigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[1] == 0.0f)
        chi2[1] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[8] == 0.0f)
        chi2[8] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[5] == 0.0f)
        chi2[5] = logLikelihood(pfd,psp);
      
      psp->sigma=sParams.sigma+deltaSigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[2] == 0.0f)
        chi2[2] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[3] == 0.0f)
        chi2[3] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[4] == 0.0f)
        chi2[4] = logLikelihood(pfd,psp);
      
      dirMin = -1;
      chi2Min = chi2[8]; // first guess at minimum 
      for(k=0, fptr=chi2;k<8;++k, ++fptr) {
		  if(DEBUG)std::cout<<"chi2["<<k<<"]="<<*fptr<<", chi2Min="<<chi2Min<<", dirMin="<<dirMin<<std::endl;;
      if(DEBUG) std::cout << "FitScurveLikelihoodFloat::fitPixel: Iteration " << pFit->nIters << std::endl;
      if(DEBUG) std::cout << "mu=" << psp->mu << ", sigma=" << psp->sigma << ", chi2=" << pFit->chi2 << std::endl;
	if(*fptr < chi2Min) {
          chi2Min = *fptr;
          dirMin = k;
        }
      }
      switch(dirMin) {
      case -1:
        deltaSigma = deltaSigma * 0.1f;
        deltaMu = deltaMu * 0.1f;
        break;
      case 0:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[0];
        chi2[3] = chi2[1];
        chi2[4] = chi2[8];
        chi2[5] = chi2[7];
        chi2[0] = chi2[1] = chi2[2] = 
          chi2[6] = chi2[7] = 0.0f;
        break;
      case 1:
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[1];
        chi2[3] = chi2[2];
        chi2[4] = chi2[3];
        chi2[5] = chi2[8];
        chi2[6] = chi2[7];
        chi2[7] = chi2[0];
        chi2[0] = chi2[1] = chi2[2] = 0.0f;
        break;
      case 2:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[2];
        chi2[5] = chi2[3];
        chi2[6] = chi2[8];
        chi2[7] = chi2[1];
        chi2[0] = chi2[1] = chi2[2] = 
          chi2[3] = chi2[4] = 0.0f;
        break;
      case 3:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        chi2[8] = chi2[3];
        chi2[5] = chi2[4];
        chi2[6] = chi2[5];
        chi2[7] = chi2[8];
        chi2[0] = chi2[1];
        chi2[1] = chi2[2];
        chi2[2] = chi2[3] = chi2[4] = 0.0f;
        break;
      case 4:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[4];
        chi2[7] = chi2[5];
        chi2[0] = chi2[8];
        chi2[1] = chi2[3];
        chi2[2] = chi2[3] = chi2[4] = 
          chi2[5] = chi2[6] = 0.0f;
        break;
      case 5:
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[5];
        chi2[7] = chi2[6];
        chi2[0] = chi2[7];
        chi2[1] = chi2[8];
        chi2[2] = chi2[3];
        chi2[3] = chi2[4];
        chi2[4] = chi2[5] = chi2[6] = 0.0f;
        break;
      case 6:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[6];
        chi2[1] = chi2[7];
        chi2[2] = chi2[8];
        chi2[3] = chi2[5];
        chi2[4] = chi2[5] = chi2[6] = 
          chi2[7] = chi2[0] = 0.0f;
        break;
      case 7:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        chi2[8] = chi2[7];
        chi2[1] = chi2[0];
        chi2[2] = chi2[1];
        chi2[3] = chi2[8];
        chi2[4] = chi2[5];
        chi2[5] = chi2[6];
        chi2[6] = chi2[7] = chi2[0] = 0.0f;
        break;
      }
    }
    
    if(deltaSigma < (pFit->sigmaEpsilon * sParams.sigma) &&
       deltaMu < (pFit->muEpsilon * sParams.mu)) {
      pFit->converge = true;
      break;
    }
  }
  psp = (GaussianCurve *)pFit->curve;
  *psp = sParams;
  pFit->chi2=chiSquared(pfd,psp)/(float) pFit->ndf;
  //if(pFit->converge && pFit->chi2>40){ std::cout<<"WARNING! Chi2="<<pFit->chi2<<", "<<pFit->ndf<<", a0="<<psp->a0<<std::endl; }
  //static int d=0;
 
    if(DEBUG)printf("FitScurveLikelihoodFloat::fitPixel output: %f %f %f %d\n",psp->mu,psp->sigma,pFit->chi2, pFit->converge);
  
  return pFit->converge;
}

float FitScurveLikelihoodFloat::chiSquared(FitData *pfd,GaussianCurve *s) {
 float acc, y0,y1,y2,y3, a0, chi2;
  const float *x, *y;
  float x0;
  int j, k, n;  
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0.0f; 
  //a0 = s->a0;  
  a0 = m_a0;  
  //std::cout<<"a0="<<a0<<std::endl;
  /* use binomial weighting */
  for(k=0;k<n;++k) {
    x0 = *x++; 
    y0 = *y++; 
    y1 = errf_ext(x0,s); 
    j = (int)(y1 * inverse_weight_lut_interval); 
    y2 = y0 - a0 * y1; 
    y3 = y2 * y2;
    acc += (y3 * FitDataFloat::binomial_weight[j]); 
  }
  chi2 = acc/ a0;
  return chi2;
}
