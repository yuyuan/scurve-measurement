#include "RCEFITTER.hh"
#include "FitData.hh"
#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <boost/math/special_functions/erf.hpp>
#include <TCanvas.h>
#include <TH2F.h>
#include "TRandom3.h"
#include "TMath.h"
#include "TF1.h"
#include <algorithm>

// 生成符合误差函数分布的样例数据集
void generateErrorFunctionData(std::vector<float>& x, std::vector<float>& y, float mu, float sigma) {
    TRandom3 random; 
    TF1 *pdf = new TF1("pdf", "500 * (1 + TMath::Erf((x-[0])/(TMath::Sqrt(2)*[1])))", 0, 1000); 
    pdf->SetParameters(mu, sigma); 
    for (int i = 0; i < 1000; ++i) {
        x[i] = i + 1;
        y[i] = pdf->Eval(x[i]);  
    }
    /*
    for(int i =550;i<1000;++i){
        y[i] = 1000;
    }
*/
    std::sort(y.begin(), y.end());
}

int main() {
    std::vector<float> x(1000, 0);
    std::vector<float> y(1000, 0);

    FitScurveLikelihoodFloat rceFitter(1000);

    // 生成符合误差函数分布的样例数据集
    float mu0 = 561;
    float sigma0 = 71.34;

    FitResult rceFitresult;
    // 创建Canvas
    TCanvas canvas("canvas", "Occupancy Maps", 800, 600);

    // 创建二维直方图
    TH2F *histogram = new TH2F("histogram", "Occupancy Map; vcal; Occupancy", 1000, 0, 1000, 100, 0, 1200);

    // 填充直方图
    for (int j= 0; j<100000; ++j){
        generateErrorFunctionData(x, y, mu0, sigma0);
        for (int i = 0; i < 1000; ++i) {
        histogram->Fill(x[i], y[i]);
        }
        if(j==99999){
            float mu,sigma;
            rceFitresult = rceFitter.doFit(x, y, mu, sigma);
            sigma=sigma/TMath::Sqrt(2);
            double chi2=0;
            chi2=rceFitresult.chi2;
        
        if (rceFitresult.converge) {
            std::cout << "Fit converged successfully!" << std::endl;
            std::cout << "mu: " << mu << " sigma: " << sigma << std::endl;
            std::cout << "chi2:" << chi2 << std::endl;
        } else {
            std::cout << "Fit did not converge!" << std::endl;
        }
        }
    }
        

    // 设置绘制样式
    histogram->SetFillColor(kBlue);
    histogram->Draw("colz");  // 使用colz选项进行颜色绘制

    // 显示Canvas
    canvas.Print("Occupancy.png");

    return 0;
}