# Scurve measurement



## RCEFITTER.hh和RCEFITTER.cpp

这个库包括了所有查找表方法拟合所需的函数，其中最重要的是doFit()和fitPixel()函数。

## FitData.hh和FitData.cpp

这个库将三个查找表连接到本文件夹中的三个dat文件。


## example.cpp

该文件为一个样例分析文件，其中generateErrorFunctionData函数利用root函数库中的TMath函数生成符合误差函数分布的变量（可以设置mu和sigma），main()函数中则实现了对该二维变量的拟合，并给出拟合得到的mu和sigma值以及绘制二维直方图。


## 运行example.cpp

首先确保你安装了root，截止到2024/1，以最新版的root为例子，前往https://root.cern/releases/release-63002/ 找到符合你系统的版本，
以Ubuntu 22.04为例，完成下载后找到安装包，通过下面的代码解压完成安装
```
tar -xzvf root_v6.30.02.Linux-ubuntu22.04-x86_64-gcc11.4.tar.gz
```
可以通过以下代码测试是否完成安装：
```
root -l
```
如果成功安装则会进入root（命令行前端出现root[0]字样）

通过以下代码运行example.cpp文件
```
g++ `root-config --cflags` -o out FitData.cpp RCEFITTER.cpp example.cpp `root-config --libs`
```

如果没有报错，你会得到名为out的可执行文件，然后执行它：
```
./out
```

即可得到此次拟合的mu和sigma值，以及得到对应的二维直方图。

如果出现root函数库相关的报错，例如找不到库函数，可以先找到你的root安装目录，例如~/Downloads/root，其中的bin文件夹下有一个文件名为thisroot.sh，可以通过source它来解决报错
```
source ~/Downloads/root/bin/thisroot.sh
```